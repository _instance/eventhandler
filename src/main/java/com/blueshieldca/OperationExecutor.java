package com.blueshieldca;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class OperationExecutor {
    private ScheduledThreadPoolExecutor scheduledThreadPoolExecutor;

    public OperationExecutor(Integer numberOfThread) {
        scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(numberOfThread);
    }

    public ScheduledFuture start(StartEventOperation operation) {
        return scheduledThreadPoolExecutor.schedule(operation, operation.getTimeOut(), TimeUnit.SECONDS);
    }

    public void shutdown() {
        scheduledThreadPoolExecutor.shutdown();
    }
}
