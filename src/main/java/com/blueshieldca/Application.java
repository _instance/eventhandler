package com.blueshieldca;

import java.io.InputStreamReader;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        ResultKeeper resultKeeper = new ResultKeeper();
        EventRegister eventRegister = new EventRegister(resultKeeper);
        Scanner scanner = new Scanner(new InputStreamReader(System.in));
        while (true) {
            System.out.print('>');
            String line = scanner.nextLine();
            if (line.matches("^((start\\s+\\S{1,100}\\s+[0-9]{1,9}$)|(approve\\s+\\S{1,100})|(exit))$")) {
                String[] s = line.split("\\s+");
                switch (s[0]) {
                    case "start":
                        eventRegister.register(s[1], Integer.parseInt(s[2]));
                        break;
                    case "approve":
                        eventRegister.approve(s[1]);
                        break;
                    case "exit":
                        eventRegister.stop();
                        resultKeeper.printResults();
                        System.exit(0);
                }
            } else {
                System.out
                        .println("Unknown command \""
                                + line
                                + "\". "
                                + "\nExample: [start <event_id> <timeout> | approve <event_id> | exit] where <event_id> any string with the length less than 100 and <timeout> integer 0..999999999");
            }

            resultKeeper.printResults();
        }
    }
}
