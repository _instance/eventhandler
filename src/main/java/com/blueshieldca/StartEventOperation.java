package com.blueshieldca;

import java.util.concurrent.ScheduledFuture;

public class StartEventOperation implements Runnable {
    private String eventName;
    private Integer timeOut;
    private EventRegister eventRegister;
    private volatile ScheduledFuture scheduledFuture;
    private volatile boolean registered = false;

    public StartEventOperation(String eventName, Integer timeOut, EventRegister eventRegister) {
        this.eventName = eventName;
        this.timeOut = timeOut;
        this.eventRegister = eventRegister;
    }

    public void setScheduledFuture(ScheduledFuture scheduledFuture) {
        this.scheduledFuture = scheduledFuture;
    }

    public void register() {
        registered = true;
    }

    public Integer getTimeOut() {
        return timeOut;
    }

    public String getEventName() {
        return eventName;
    }

    @Override
    public void run() {
        /**
         * Wait until operation is registered
         */
        while (!registered);

        if (eventRegister.unregister(eventName, scheduledFuture)) {
            eventRegister.addFailedEvent(eventName);
        }
    }
}
