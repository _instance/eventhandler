package com.blueshieldca;

import java.io.PrintStream;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Container for keeping results about event execution
 */
public class ResultKeeper {
    private PrintStream resultStream;
    private Queue<String> queue;

    public ResultKeeper() {
        this(System.out);
    }

    public ResultKeeper(PrintStream printStream) {
        this.resultStream = printStream;
        this.queue = new ConcurrentLinkedQueue<>();
    }

    public boolean addResult(String result) {
        return queue.offer(result);
    }

    /**
     * Prints result
     *
     * @return number of printed results
     */
    public int printResults() {
        String result;
        int i = 0;
        while ((result = queue.poll()) != null) {
            resultStream.println(result);
            i++;
        }
        return i;
    }
}
