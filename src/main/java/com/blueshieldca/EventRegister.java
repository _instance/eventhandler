package com.blueshieldca;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;

public class EventRegister {

    private ConcurrentHashMap<String, ScheduledFuture> events;
    private ResultKeeper resultKeeper;
    private OperationExecutor operationExecutor;

    public EventRegister(ResultKeeper resultKeeper) {
        this(resultKeeper, Runtime.getRuntime().availableProcessors());
    }

    public EventRegister(ResultKeeper resultKeeper, int numberOfThreads) {
        this.events = new ConcurrentHashMap<>();
        this.resultKeeper = resultKeeper;
        this.operationExecutor = new OperationExecutor(numberOfThreads);
    }

    /**
     * Schedules event start operation and puts event in register. Also the method cancels event with the name <code>eventName</code> if
     * it was registered before.
     * @param eventName event name
     * @param timeout timeout when event is failed
     * @return <code>false</code> when event with the same name <code>eventName</code> was registered previously, <code>true</code> otherwise.
     */
    public boolean register(String eventName, Integer timeout) {
        StartEventOperation operation = new StartEventOperation(eventName, timeout, this);

        ScheduledFuture scheduledFuture = operationExecutor.start(operation);
        operation.setScheduledFuture(scheduledFuture);

        scheduledFuture = events.put(operation.getEventName(), scheduledFuture);
        /**
         * It is needed to handle a case when operation is executed before it is put in map.
         * For example it is possible when timeout value is too little.
         */
        operation.register();

        if (scheduledFuture != null) {
            if (scheduledFuture.cancel(false)) {
                //addResult("<" + "previous event " + operation.getEventName() + " was cancelled");
            }
            return false;
        }
        return true;
    }

    public boolean approve(String eventName) {
        ScheduledFuture scheduledFuture = events.remove(eventName);
        if (scheduledFuture != null) {
            if (scheduledFuture.cancel(false)) {
                addApprovedEvent(eventName);
                return true;
            }
        }
        return false;
    }

    public boolean unregister(String eventName, ScheduledFuture scheduledFuture) {
        return events.remove(eventName, scheduledFuture);
    }

    public void stop() {
        operationExecutor.shutdown();
    }

    void addFailedEvent(String eventName) {
        addResult("<" + eventName + " is not approved");
    }

    private void addApprovedEvent(String eventName) {
        addResult("<" + eventName + " is approved");
    }

    private void addResult(String result) {
        resultKeeper.addResult(result);
    }


}


