package com.blueshieldca;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.concurrent.ScheduledFuture;

public class StartEventOperationTest {

    private EventRegister eventRegister;

    @Before
    public void setUp() throws Exception {
        eventRegister = Mockito.mock(EventRegister.class);
        Mockito.when(eventRegister.unregister(Mockito.anyString(), Mockito.any(ScheduledFuture.class))).thenReturn(true);
    }

    @Test
    public void testRun() throws Exception {
        String eventName = "event";
        Integer timeout = 1;
        StartEventOperation startEventOperation = new StartEventOperation(eventName, timeout, eventRegister);
        startEventOperation.register();
        startEventOperation.run();
        Mockito.verify(eventRegister).addFailedEvent(eventName);
    }


}