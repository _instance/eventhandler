package com.blueshieldca;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ResultKeeperTest {

    private ResultKeeper resultKeeper;
    private ExecutorService executorService;

    @Before
    public void setUp() {
        executorService = Executors.newFixedThreadPool(5);
    }

    @After
    public void tearDown() {
        executorService.shutdown();
    }

    @Test
    public void printTest() throws Exception {
        PrintStream printStream = Mockito.mock(PrintStream.class);
        resultKeeper = new ResultKeeper(printStream);

        int i = 10;
        final String result = "event result";
        List<Future> futureList = new ArrayList<>(i);


        for (int j = i; j > 0; j--) {
            Future f = executorService.submit(new Runnable() {
                @Override
                public void run() {
                    resultKeeper.addResult(result);
                }
            });
            futureList.add(f);
        }

        for (Future future : futureList) {
            future.get();
        }
        Assert.assertEquals(i, resultKeeper.printResults());
        Mockito.verify(printStream, Mockito.times(i)).println(result);
    }
}
