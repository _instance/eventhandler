package com.blueshieldca;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.PrintStream;
import java.util.concurrent.TimeUnit;

public class EventRegisterTest {

    private EventRegister eventRegister;
    private ResultKeeper resultKeeper;
    private PrintStream printStream;
    private String name;
    private Integer timeout;

    @Before
    public void setUp() throws Exception {
        printStream = Mockito.mock(PrintStream.class);
        resultKeeper = new ResultKeeper(printStream);
        eventRegister = new EventRegister(resultKeeper);
    }

    @After
    public void tearDown() throws Exception {
        eventRegister.stop();
    }

    @Test
    public void testRegister() throws Exception {
        name = "event";
        timeout = 10;
        Assert.assertTrue(eventRegister.register(name, timeout));
        Assert.assertFalse(eventRegister.register(name, timeout - 1));
    }

    @Test
    public void testApprove() throws Exception {
        name = "event";
        timeout = 10;
        Assert.assertTrue(eventRegister.register(name, timeout));
        Assert.assertTrue(eventRegister.approve(name));
        Assert.assertEquals(1, resultKeeper.printResults());
        Mockito.verify(printStream).println("<" + name + " is approved");
    }

    @Test
    public void testApproveTimeout() throws Exception {
        String name = "event";
        Integer timeout = 2;
        Assert.assertTrue(eventRegister.register(name, timeout));
        TimeUnit.SECONDS.sleep(timeout + 1);
        Assert.assertFalse(eventRegister.approve(name));
        Assert.assertEquals(1, resultKeeper.printResults());
        Mockito.verify(printStream).println("<" + name + " is not approved");
    }
}